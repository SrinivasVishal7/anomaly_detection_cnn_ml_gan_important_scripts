import cv2
import numpy as np
from keras.models import load_model
from keras.applications import MobileNet
from keras_squeezenet import SqueezeNet
from sklearn.externals import joblib
from keras.preprocessing import image
from keras.applications.imagenet_utils import preprocess_input
from important_scripts.resize_images import resize_image
import argparse

from important_scripts.extractpreprocessortwoimagesblur import ExtractPreprocessorTwoImagesBlur

crop_w = 144
crop_h = 144

cam = cv2.VideoCapture(1)
if not cam.isOpened():
    print('camera not opened')
    exit()
else:
    print('camera opened')

ap = argparse.ArgumentParser()

ap.add_argument('-p', "--Path", required=True,
                help="path to model")

ap.add_argument("-m", "--model", default="0",
                help="0-mobilenet model, 1-squeezenet model, 2-Custom path for model")

ap.add_argument("-d", "--mode", default="0",
                help="0 - Single class Anomaly, 1 - multi class anomaly")

ap.add_argument("-mh", "--model_path", default=None, help="Give path to custom feature extractor")

args = vars(ap.parse_args())

if args["model"] is "2" and args["model_path"] is None:
    raise Exception("Give model path")

print(args["model"])

model = {
    "0": MobileNet(input_shape=(128, 128, 3), weights='imagenet', include_top=False),
    "1": SqueezeNet(input_shape=(128, 128, 3), weights='imagenet', include_top=False),
    #"2": load_model(args["model_path"])
}

print("\nLoading pre-trained model...\n")

cam.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

lsanomaly_model = joblib.load(args['Path'])
anomaly_labels = ['Coin', 'Not a coin']

if args["mode"] is "1":
    anomaly_labels = ['Five', 'One', 'Two', 'Ten', 'Not a coin']

score = [None] * (len(anomaly_labels))
print('labels : ', anomaly_labels)

NUM_CLASSES = len(anomaly_labels)

imageCount = 0

for skip in range(10):
    ret, image_cam = cam.read()
    if ret is False:
        print("image not found")
        exit()
imgX, imgY = image_cam.shape[1] / 2, image_cam.shape[0] / 2

croppedImage = image_cam

first_gray = cv2.cvtColor(croppedImage, cv2.COLOR_BGR2GRAY)
extImage = ExtractPreprocessorTwoImagesBlur(GRAYBaseFrame=first_gray, thresholdValue=10, H=crop_h, W=crop_w)


def prediction(image_cam):
    tempImage = image_cam.copy()

    tempImage[int(imgY) - int(crop_h / 2), int(imgX) - int(crop_w / 2):int(imgX) + int(crop_w / 2)] = 0
    tempImage[int(imgY) + int(crop_h / 2), int(imgX) - int(crop_w / 2):int(imgX) + int(crop_w / 2)] = 0
    tempImage[int(imgY) - int(crop_h / 2):int(imgY) + int(crop_h / 2), int(imgX) + int(crop_w / 2)] = 0
    tempImage[int(imgY) - int(crop_h / 2):int(imgY) + int(crop_h / 2), int(imgX) - int(crop_w / 2)] = 0
    status, extractedImage = extImage.preprocess(image_cam)
    cv2.imshow("extractedImage", extractedImage)

    image_extracted = extractedImage.copy()

    if args["model"] is not "2":
        image_extracted = resize_image(image_extracted, 128, 128)

    if NUM_CLASSES < 20:
        num_row = int(NUM_CLASSES % 20)
    else:
        num_row = 19

    img = image.img_to_array(image_extracted)
    img = np.expand_dims(img, axis=0)
    img = preprocess_input(img)
    features = model[args["model"]].predict(img)
    lsanomaly_features = features.flatten()
    Y = np.array(lsanomaly_features)
    Y = Y.reshape(1, -1)
    anomaly_preds = lsanomaly_model.predict_proba(Y)
    anomaly_pred = np.argmax(anomaly_preds, axis=1)
    print(anomaly_preds)
    cv2.putText(tempImage, anomaly_labels[int(anomaly_pred)], (300, 50), 0, 0.8, (255, 0, 0), 2, cv2.LINE_AA)
    cv2.imshow("Prediction", tempImage)
    k = cv2.waitKey(1)
    if chr(k & 255) is 'q':
        exit()


while True:
    ret, image_from_cam = cam.read()
    if ret is False:
        print("image not found")
        exit()
    prediction(image_from_cam)
