# import the necessary packages
import numpy as np
import cv2


class ExtractPreprocessor:
    def __init__(self, GRAYBaseFrame, thresholdValue, H, W):
        self.GRAYBaseFrame = GRAYBaseFrame
        self.thresholdValue = thresholdValue
        self.H = H
        self.W = W
        self.initialWidth = len(GRAYBaseFrame[0])
        self.initialHeight = len(GRAYBaseFrame)

    def preprocess(self, RGBFrame):
        largest_area = 0
        GRAYFrame = cv2.cvtColor(RGBFrame, cv2.COLOR_BGR2GRAY)
        subImg = cv2.subtract(GRAYFrame, self.GRAYBaseFrame)
        ret, thresholded_frame = cv2.threshold(subImg, self.thresholdValue, 255, 0)

        kernel = np.ones((3, 3), np.uint8)
        eroded_frame_1 = cv2.erode(thresholded_frame, kernel, iterations=3)
        dilated_frame = cv2.dilate(eroded_frame_1, kernel, iterations=3)
        eroded_frame_2 = dilated_frame.copy()
        # cv2.imshow('eroded_frame_2', eroded_frame_2)
        # cv2.imshow ('eroded_frame_2', eroded_frame_2)
        res = cv2.bitwise_and(RGBFrame, RGBFrame, mask=eroded_frame_2)
        # cv2.imshow('res', res)
        res_x_cen = int(len(res[0]) / 2)
        res_y_cen = int(len(res) / 2)

        center_crop = RGBFrame[res_y_cen - int(self.H / 2):res_y_cen + int(self.H / 2),
                      res_x_cen - int(self.W / 2):res_x_cen + int(self.W / 2)]
        contours = cv2.findContours(eroded_frame_2.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[1]
        x = y = w = h = 0
        for i, cnt in enumerate(contours):
            area = cv2.contourArea(cnt)
            if area > largest_area:
                x, y, w, h = cv2.boundingRect(cnt)
                largest_area = area;
                largest_contour = cnt
                largest_contour_index = i;
        if x is 0 and y is 0 and w is 0 and h is 0:
            return False, center_crop
        else:
            x_center = int(x + w / 2)
            y_center = int(y + h / 2)

            x1, x2 = x_center - int(self.W / 2), x_center + int(self.W / 2)
            y1, y2 = y_center - int(self.H / 2), y_center + int(self.H / 2)

            if x1 < 0:
                x1 = 0
            if y1 < 0:
                y1 = 0
            if x2 >= self.initialWidth:
                x2 = self.initialWidth - 1
            if y2 >= self.initialHeight:
                y2 = self.initialHeight - 1
            finalImage = res[y1:y2, x1:x2]
            #			print ('y1:y2, x1:x2 : ', y1, y2, x1, x2)
            if y2 - y1 is self.H and x2 - x1 is self.W:
                return True, finalImage
            else:
                return False, center_crop
