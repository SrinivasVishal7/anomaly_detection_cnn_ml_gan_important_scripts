import os
import random
import shutil

train_dir = '/home/hackathon/Srinivas_Projects/projects_AI/keras-yolo2/train_image_folder'
annotate_dir = '/home/hackathon/Srinivas_Projects/projects_AI/keras-yolo2/train_annotation_folder'
n = 55
while n != 0:
    filename = random.choice(os.listdir(train_dir))
    train_image_path = os.path.join(train_dir, filename)
    annotation_name = os.path.splitext(os.path.basename(train_image_path))[0] + '.xml'
    train_annotation_path = os.path.join(annotate_dir, annotation_name)
    shutil.move(train_image_path, '/home/hackathon/Srinivas_Projects/projects_AI/keras-yolo2/validation_image_folder')
    shutil.move(train_annotation_path, '/home/hackathon/Srinivas_Projects/projects_AI/keras-yolo2'
                                       '/validation_annotation_folder')
    n = n - 1
