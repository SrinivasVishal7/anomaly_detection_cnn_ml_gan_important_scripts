import numpy as np
import argparse
import imutils
import cv2
import os
from imutils import paths

ap = argparse.ArgumentParser()

ap.add_argument("-d", "--dataset", required=True,
                help="path to input dataset")

ap.add_argument("-o", "--output", required=True,
                help="path to save the rotated images")

ap.add_argument("-n", "--number", required=True,
                help="Number of Images")
args = vars(ap.parse_args())

imagePaths = list(paths.list_images(args["dataset"]))
number = int(args["number"])
num_files = len(imagePaths)
number_of_rotations = number / num_files
angleOfRotation = 360 / number_of_rotations

imageCount = 0
for im in imagePaths:
    image = cv2.imread(im)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (5, 5), 0)
    ret3, thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    image = cv2.bitwise_and(image, image, mask=thresh)
    if image is None:
        continue

    fileName = args["output"].split(os.path.sep)[-1]
    for angle in np.arange(0, 360, angleOfRotation):
        rotated = imutils.rotate(image, angle)
        cv2.imwrite(os.path.sep.join([args["output"], "rotated_" + str(angle) + "_" + str(imageCount) + ".bmp"]),
                    rotated)
        imageCount += 1
        if imageCount == number:
            break
