import cv2
import argparse
import os


def resize_image(image, resize_height, resize_width):
    height, width = image.shape[:2]
    scaling_factor = resize_height / float(height)
    if resize_width / float(width) < scaling_factor:
        scaling_factor = resize_width / float(width)
    return cv2.resize(image, None, fx=scaling_factor, fy=scaling_factor, interpolation=cv2.INTER_AREA)


def main():
    ap = argparse.ArgumentParser()

    ap.add_argument("-p", "--input", required=True,
                    help="path to input image directory")

    ap.add_argument("-o", "--output", default="0",
                    help="path to output directory")

    args = vars(ap.parse_args())
    input_path = args["input"]
    output_path = args["output"]

    for subdir, dirs, files in os.walk(input_path):
        for filename in files:
            if filename.endswith(".bmp"):
                filename_full = os.path.join(subdir, filename)
                img = cv2.imread(filename_full)
                filename_new = os.path.join(output_path, filename)
                cv2.imwrite(filename_new, resize_image(img, 128, 128))


if __name__ == "__main__":
    main()
