import cv2
import numpy as np
from keras.applications import VGG16
from keras.applications.imagenet_utils import preprocess_input, decode_predictions

label = ''
frame = None

import sys
import threading


class predictionThread():
    def __init__(self):
        threading.Thread.__init__(self)
        self.model = VGG16(weights="imagenet")

    def run(self):
        global label
        global fps
        print("[INFO] loading network...")

        while ~(frame is None):
            label = self.predict(frame)

        if frame is None:
            sys.exit()

    def predict(self, frame):
        image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB).astype(np.float32)
        image = image.reshape((1,) + image.shape)
        image = preprocess_input(image)
        preds = self.model.predict(image)
        return decode_predictions(preds)[0]


cap = cv2.VideoCapture(1)

if cap.isOpened():
    print("Camera Fine")
else:
    cap.open()

fps = cap.get(cv2.CAP_PROP_FPS)

keras_thread = predictionThread()
#keras_thread.start()

while True:
    ret, original = cap.read()

    original = cv2.resize(original, (224, 224))

    keras_thread.predict(original)

    cv2.putText(original, "FPS: {}".format(fps), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.9,
                (255, 0, 0), 2)
    cv2.imshow("Classification", original)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
frame = None
cv2.destroyAllWindows()
sys.exit()
