import queue

import cv2
import numpy as np
from keras.applications import VGG16
from keras.applications.imagenet_utils import preprocess_input, decode_predictions
import time

label = ''
frame = None

import sys
import threading
from queue import Queue


class predictionThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.prediction_time = None
        self._queue = queue

    def run(self):
        global label
        print("[INFO] loading network...")
        self.model = VGG16(weights="imagenet")

        while ~(frame is None):
            self.prediction_time, label = self.predict(frame)
            print("{} seconds".format(self.prediction_time))

        if frame is None:
            sys.exit()

    def predict(self, frame):
        image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB).astype(np.float32)
        image = image.reshape((1,) + image.shape)
        image = preprocess_input(image)
        start_time = time.time()
        preds = self.model.predict(image)
        prediction = time.time() - start_time
        return prediction, decode_predictions(preds)[0]


cap = cv2.VideoCapture(1)

if cap.isOpened():
    print("Camera Fine")
else:
    cap.open()

fps = cap.get(cv2.CAP_PROP_FPS)

keras_thread = predictionThread()
keras_thread.start()

while True:
    ret, original = cap.read()

    frame = cv2.resize(original, (224, 224))

    cv2.putText(original, "label : {}".format(label), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.9,
                (255, 0, 0), 2)
    cv2.imshow("Main Frame", original)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
frame = None
cv2.destroyAllWindows()
sys.exit()
