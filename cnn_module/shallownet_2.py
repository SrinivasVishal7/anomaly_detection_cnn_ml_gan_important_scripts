# USAGE
# python finetune_flowers17.py --dataset ../datasets/flowers17/images \
# 	--model flowers17.model

# import the necessary packages
from cnn_module.config import coin_1_training_1_config as config

from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report

from cnn_module.pyimagesearch.nn.conv import ShallowNet_2
from cnn_module.pyimagesearch.io import HDF5DatasetGenerator
from cnn_module.pyimagesearch.io import HDF5DatasetGeneratorFineTune
from cnn_module.pyimagesearch.callbacks import EpochCheckpoint
from cnn_module.pyimagesearch.callbacks import TrainingMonitor_SavingHistory

from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import SGD
from keras.utils import plot_model
from keras.applications import VGG16
from keras.layers import Input
from keras.models import Model

from imutils import paths
import numpy as np
import argparse
import os
import pickle

# from pyimagesearch.io import HDF5DatasetGeneratorFineTune


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-m", "--model", type=str,
                help="path to *specific* model checkpoint to load")
ap.add_argument("-s", "--start_epoch", type=int, default=0,
                help="epoch to restart training at")
ap.add_argument("-c", "--checkpoints", required=True,
                help="path to output checkpoint directory")
args = vars(ap.parse_args())

NUM_EPOCHS = 1000
INIT_LR = 1e-5
BATCH_SIZE = 10

fileName = __file__.split(os.path.sep)[-1].split(".")[-2]

trainGen = HDF5DatasetGeneratorFineTune(config.TRAIN_HDF5, BATCH_SIZE, classes=config.NUM_CLASSES)
valGen = HDF5DatasetGeneratorFineTune(config.VAL_HDF5, BATCH_SIZE, classes=config.NUM_CLASSES)

print("[INFO] compiling model...")
model = ShallowNet_2.build(width=144, height=144, depth=3, classes=4)
opt = SGD(lr=INIT_LR)
model.compile(loss="categorical_crossentropy", optimizer=opt,
              metrics=["accuracy"])
plot_model(model, to_file="output/network_" + fileName + ".png", show_shapes=True)

# construct the set of callbacks
directoryName = os.path.sep.join([args["checkpoints"], fileName])
if not os.path.exists(directoryName):
    os.makedirs(directoryName)

f = open(os.path.sep.join([directoryName, 'ClassLabels.cpickle']), "wb")
f.write(pickle.dumps(trainGen.labels_map[:]))
f.close()

callbacks = [
    EpochCheckpoint(directoryName, every=5,
                    startAt=args["start_epoch"]),
    TrainingMonitor_SavingHistory("output/" + fileName + ".png",
                                  os.path.sep.join([directoryName, fileName + '_history']),
                                  jsonPath="output/" + fileName + ".json",
                                  startAt=args["start_epoch"])]

# train the network
print("Generating History")

history = model.fit_generator(
    trainGen.generator(),
    steps_per_epoch=trainGen.numImages // BATCH_SIZE,
    validation_data=valGen.generator(),
    validation_steps=valGen.numImages // BATCH_SIZE,
    epochs=NUM_EPOCHS,
    max_q_size=BATCH_SIZE * 2,
    callbacks=callbacks, verbose=1)

# with open('history', 'wb') as file_pi:
#	pickle.dump(history.history, file_pi)
# print("[INFO] serializing model...")
# model.save(args["model"])

# evaluate the network on the fine-tuned model
# print("[INFO] evaluating after fine-tuning...")
# predictions = model.predict(testX, batch_size=BATCH_SIZE)
# print(classification_report(testY.argmax(axis=1),
#	predictions.argmax(axis=1), target_names=classNames))
