import argparse
import os

from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras import backend as K

from cnn_module.pyimagesearch.callbacks import EpochCheckpoint, TrainingMonitor_SavingHistory

img_width, img_height = 300, 300

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset_path", required=True,
                help="Dataset directory")

args = vars(ap.parse_args())


train_data_dir = os.path.join(args["dataset_path"], "train")
validation_data_dir = os.path.join(args["dataset_path"], "validation")
nb_train_samples = 3000
nb_validation_samples = 1000
epochs = 50
batch_size = 16

if K.image_data_format() == 'channels_first':
    input_shape = (3, img_width, img_height)
else:
    input_shape = (img_width, img_height, 3)

model = Sequential()
model.add(Conv2D(32, (3, 3), input_shape=input_shape))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

model.add(Conv2D(64, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

model.add(Flatten())
model.add(Dense(64))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(4))
model.add(Activation('softmax'))

model.compile(loss='categorical_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])

train_datagen = ImageDataGenerator(
    rescale=1. / 255,
    shear_range=0.2)

test_datagen = ImageDataGenerator(rescale=1. / 255)

callbacks = [
    EpochCheckpoint("output", every=5,
                    startAt=10),
    TrainingMonitor_SavingHistory("output/model_shallownet_vishal.png",
                                  os.path.sep.join(["output", "model_shallownet_vishal" + '_history']),
                                  jsonPath="output/" + "model_shallownet_vishal.png" + ".json",
                                  startAt=10)]

train_generator = train_datagen.flow_from_directory(
    train_data_dir,
    target_size=(img_width, img_height),
    batch_size=batch_size,
    class_mode='categorical')

validation_generator = test_datagen.flow_from_directory(
    validation_data_dir,
    target_size=(img_width, img_height),
    batch_size=batch_size,
    class_mode='categorical')

model.fit_generator(
    train_generator,
    steps_per_epoch=nb_train_samples // batch_size,
    epochs=epochs,
    validation_data=validation_generator,
    validation_steps=nb_validation_samples // batch_size,
    callbacks=callbacks)

model.save_weights('coin_model_weights.h5')
model.save('coin_model.h5')