# USAGE
# python extract_features.py --dataset ../datasets/animals/images \
# 	--output ../datasets/animals/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/caltech101/images \
# 	--output ../datasets/caltech101/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/flowers17/images \
#	--output ../datasets/flowers17/hdf5/features.hdf5

# import the necessary packages
from keras.applications import VGG16
from keras.applications import imagenet_utils
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img
from sklearn.preprocessing import LabelEncoder
from pyimagesearch.io import HDF5DatasetWriter
from imutils import paths
import numpy as np
import progressbar
import argparse
import random
import os
import cv2
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
import argparse
import pickle
import h5py
#from pyimagesearch.preprocessing import ExtractPreprocessor
from pyimagesearch.preprocessing import ExtractPreprocessorTwoImagesBlur as EP
from keras.models import load_model



# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()

ap.add_argument("-m", "--model", required=True,
	help="path to model")
ap.add_argument("-o", "--output", default=None,
	help="path to model")
ap.add_argument("-r", "--ref", default=None,
	help="path to model")
args = vars(ap.parse_args())

crop_w = 144
crop_h = 144

WIDTH  = 640
HEIGHT = 480

MARGIN = 50

# load the VGG16 network
print("[INFO] loading network...")
cam = cv2.VideoCapture (0)
if not cam.isOpened():
	print ('camera not opened')
	exit()
else:
	print ('camera opened')

cam.set (cv2.CAP_PROP_FRAME_WIDTH, 640)
cam.set (cv2.CAP_PROP_FRAME_HEIGHT, 480)


model_1 = load_model(args["model"])

fileName = args["model"].split(".")
print ('fileName[-2]  : ',fileName[-2])
dirName = fileName[-2].split(os.path.sep)
dirName[-1] = "ClassLabels"
dirName = os.path.sep.join(dirName)
print ('dirName : ',dirName)
#fileName [-1] = "cpickle"
fileName = ".".join([dirName, "cpickle"])

f = open(fileName, "rb")
labels = pickle.load(f)
print ('classes : ', len (labels))
score = [None]*(len(labels))
print ('labels : ',labels)
f.close()


refImages = [None]*(len(labels))
refImgCount = 0
for lb in labels:
	refPath = os.path.sep.join([args['ref'], lb])
	refImgPath = list(paths.list_images(refPath))
	refImages[refImgCount] = cv2.imread(refImgPath[0])
	refImgCount += 1

imageCount = 0
whiteOnBblack = True
thresVal = 1

for skip in range (10):
	ret, image = cam.read()
	if ret is False:
		print ("image not found")
		exit()
imgX, imgY = image.shape[1]/2, image.shape[0]/2

croppedImage = image

first_gray = cv2.cvtColor(croppedImage, cv2.COLOR_BGR2GRAY)
extImage = EP(GRAYBaseFrame=first_gray, thresholdValue=thresVal, H=crop_h, W=crop_w, whiteOnBblack=whiteOnBblack)

cv2.namedWindow ('croppedImage', )
cv2.namedWindow ('probabilities', )
cv2.namedWindow ('frame', cv2.WINDOW_NORMAL)
cv2.namedWindow ('reference image', )
#cv2.resizeWindow ('frame', int(1280/4), int(720/4))

tempImage = image.copy()

tempImage[int(imgY)-int(crop_h/2), int(imgX)-int(crop_w/2):int(imgX)+int(crop_w/2)] = 0
tempImage[int(imgY)+int(crop_h/2), int(imgX)-int(crop_w/2):int(imgX)+int(crop_w/2)] = 0
tempImage[int(imgY)-int(crop_h/2):int(imgY)+int(crop_h/2), int(imgX)+int(crop_w/2)] = 0
tempImage[int(imgY)-int(crop_h/2):int(imgY)+int(crop_h/2), int(imgX)-int(crop_w/2)] = 0

def fineTunePrediction (image):
	fullImage = image.copy()

	#image = image [int(imgY)-int(crop_h/2)-MARGIN:int(imgY)+int(crop_h/2)+MARGIN, int(imgX)-int(crop_w/2)-MARGIN:int(imgX)+int(crop_w/2)+MARGIN]

#	status, extractedImage = extImage.preprocess (image)
	status, extractedImage, _ = extImage.preprocess (image)
	cv2.imshow("extractedImage", extractedImage)
	#print ('status : {}'.format(status))
	#if extractedImage is F:
	#extractedImage = tempImage[int(imgY)-int(crop_h/2):int(imgY)+int(crop_h/2), int(imgX)-int(crop_w/2):int(imgX)+int(crop_w/2)]

	image = extractedImage.copy()
	#canvas = np.zeros((35*len(labels)+5, 300, 3), dtype="uint8")
	canvas = np.zeros((35*20+5, 350*2, 3), dtype="uint8")
	croppedImage = image.copy()
	image = np.expand_dims(image, axis=0)

	features = image
	#preds = model_1.predict_proba(features, verbose=0)
	preds = model_1.predict(features, verbose=0)
	pred = np.argmax(preds, axis=1)
	for cl in range(len(labels)):
	#		print (cl)
		val = preds[0][cl]*100
		score[cl] = round(val, 2)
	best_score = score[int(pred)]

	for cl in range(len(labels)):
		#if cl < 15:
		w = int (score[cl]*3 )
		if cl == pred:
			cv2.rectangle(canvas, (5+350*int(cl/20), (int(cl%20) * 35) + 5), (w+5+350*int(cl/20), (int(cl%20) * 35) + 35), (255, 0, 0), -1)
		else:
			cv2.rectangle(canvas, (5+350*int(cl/20), (int(cl%20) * 35) + 5), (w+5+350*int(cl/20), (int(cl%20) * 35) + 35), (0, 0, 255), -1)
		cv2.putText(canvas, labels[int(cl)]+' '+str(score[cl]), (10+400*int(cl/20), (int(cl%20) * 35) + 23), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (255, 255, 255), 1)

	#os.system("espeak labels[int(pred)]")
	cv2.putText(tempImage, labels[int(pred)]+' '+str(best_score), (300, 50), 0, 0.8, (255, 0, 0), 2, cv2.LINE_AA)

	cv2.imshow ('croppedImage', croppedImage)
	cv2.imshow ('probabilities', canvas)
	cv2.imshow ('frame', fullImage)
	cv2.resizeWindow ('frame', int(WIDTH/4), int(HEIGHT/4))
	cv2.imshow ('reference image', refImages[int(pred)])
	k = cv2.waitKey(1)
	if chr(k&255) is 'q':
		return exit()
	elif chr(k&255) is 's':
		if args['output'] is not None:
			cv2.imwrite (os.path.sep.join([args['output'], str(imageCount)+"_full.bmp"]),        fullImage)
			#if extractedImage is not None:
			cv2.imwrite (os.path.sep.join([args['output'], str(imageCount)+".bmp"]),             tempImage)
			cv2.imwrite (os.path.sep.join([args['output'], str(imageCount)+"_probability.bmp"]), canvas)
			cv2.imwrite (os.path.sep.join([args['output'], str(imageCount)+"_cropped.bmp"]),     croppedImage)
			imageCount += 1
	return pred
lastPred = None
while True:
	ret, image = cam.read()
	if ret is False:
		print ("image not found")
		exit()
	presentPred = fineTunePrediction(image)

	if lastPred != presentPred:
		command = "espeak \'{}\'".format(str(labels[int(presentPred)]))
		os.system(command)
	lastPred = presentPred

#f.close()
