# import the necessary packages
from keras.models import Sequential
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Dropout
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dense
from keras import backend as K

class ShallowNet_2:
    @staticmethod
    def build(width, height, depth, classes):
        # initialize the model along with the input shape to be
        # "channels last"
        model = Sequential()
        inputShape = (height, width, depth)

        # if we are using "channels first", update the input shape
        if K.image_data_format() == "channels_first":
            inputShape = (depth, height, width)

        # define the first (and only) CONV => RELU layer
        model.add(Conv2D(16, (3, 3), padding="same", strides=(2, 2), input_shape=inputShape))
        model.add(Activation("relu"))
        #		model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

        model.add(Conv2D(16, (3, 3), padding="same", strides=(2, 2)))
        model.add(Activation("relu"))
        #		model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

        model.add(Conv2D(32, (3, 3), padding="same", strides=(2, 2)))
        model.add(Activation("relu"))
        #		model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

        model.add(Conv2D(32, (3, 3), padding="same", strides=(2, 2)))
        model.add(Activation("relu"))
        #		model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

        # softmax classifier
        model.add(Flatten())
        model.add(Dense(32))
        model.add(Dropout(0.5))
        model.add(Dense(classes))
        model.add(Activation("softmax"))

        # return the constructed network architecture
        return model
