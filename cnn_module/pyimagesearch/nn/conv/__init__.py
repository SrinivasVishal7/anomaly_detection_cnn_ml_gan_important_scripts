# import the necessary packages
from .shallownet import ShallowNet
from .lenet import LeNet
from .minivggnet import MiniVGGNet
from .shallownet_2 import ShallowNet_2
