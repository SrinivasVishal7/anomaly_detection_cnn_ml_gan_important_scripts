# USAGE
# python extract_features.py --dataset ../datasets/animals/images \
# 	--output ../datasets/animals/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/caltech101/images \
# 	--output ../datasets/caltech101/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/flowers17/images \
#	--output ../datasets/flowers17/hdf5/features.hdf5

# import the necessary packages
from imutils import paths
import numpy as np
import argparse
import os
import cv2
import argparse
from pyimagesearch.preprocessing import ExtractPreprocessorTwoImagesBlur as EP
import sys
import imutils
#import progressbar
#import random
import os
import cv2
import pickle
import h5py
from keras.models import load_model
import datetime
from keras.utils import plot_model
### Multiprocessing
import multiprocessing as mp
from multiprocessing import Queue, Value
from keras.models import Model
from keras import backend as K
from keras.layers import Input

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
#ap.add_argument("-d", "--dataset", required=True,
#	help="path to input dataset")
ap.add_argument("-m", "--model", required=True,
	help="path to output model")
args = vars(ap.parse_args())


#modelPath = 'model/shallownet_2/epoch_1000.hdf5'
model = load_model(args["model"])
for layer in model.layers:
	print ("{} : {}".format(layer, layer.name))


plot_model(model, to_file="test/model.png", show_shapes=True)	
model_HL = Model(inputs=model.input, outputs=model.layers[7].output)
model_HL.save("test/model_HL.hdf5")


model_HL_loaded = load_model("test/model_HL.hdf5")
plot_model(model_HL_loaded, to_file="test/model_HL.png", show_shapes=True)



DL_input = Input(model.layers[8].input_shape[1:])
DL_output = DL_input
for layer in model.layers[8:]:
	DL_output = layer(DL_output)
    
model_DL = Model(inputs=DL_input, outputs=DL_output)
model_DL.save("test/model_DL.hdf5")


model_DL_loaded = load_model("test/model_DL.hdf5")
plot_model(model_DL_loaded, to_file="test/model_DL.png", show_shapes=True)

