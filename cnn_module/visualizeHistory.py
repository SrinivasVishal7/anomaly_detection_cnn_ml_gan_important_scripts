# Visualize training history
import matplotlib.pyplot as plt
import numpy
import pickle
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-m", "--history", required=True,
	help="path to history file")
args = vars(ap.parse_args())

history = []
with (open(args["history"], "rb")) as openfile:
	while True:
		try:
			history.append(pickle.load(openfile))
		except EOFError:
			break

# list all data in history
#print(history)

#plt.figure(1)

plt.subplot(211)
# summarize history for accuracy
plt.plot(history[0]['acc'])
plt.plot(history[0]['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
#plt.show()

plt.subplot(212)
# summarize history for loss
plt.plot(history[0]['loss'])
plt.plot(history[0]['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')

#plt.legend(['train_acc', 'test_acc', 'train_loss', 'test_loss'], loc='upper left')

plt.show()
