import argparse
import os

from keras_squeezenet import SqueezeNet
from keras.preprocessing.image import ImageDataGenerator

from cnn_module.pyimagesearch.callbacks import EpochCheckpoint, TrainingMonitor_SavingHistory


ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset_path", required=True,
                help="Dataset directory")

args = vars(ap.parse_args())

img_width, img_height = 300, 300

train_data_dir = os.path.join(args["dataset_path"], "train")
validation_data_dir = os.path.join(args["dataset_path"], "validation")
nb_train_samples = 3000
nb_validation_samples = 1000
epochs = 50
batch_size = 16

if K.image_data_format() == 'channels_first':
    input_shape = (3, img_width, img_height)
else:
    input_shape = (img_width, img_height, 3)
