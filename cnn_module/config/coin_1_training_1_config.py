# define the paths to the images directory
TRAINING_IMAGES_PATH = "/home/prabu-test/MachineLearning/Akhil/coinDemo/dataset/coin_1/training_1/train"
VALIDATION_IMAGES_PATH = "/home/prabu-test/MachineLearning/Akhil/coinDemo/dataset/coin_1/training_1/validation"

NUM_CLASSES = 4

# define the path to the output training, validation, and testing
# HDF5 files

TRAIN_HDF5 = "/home/prabu-test/MachineLearning/Akhil/coinDemo/dataset/coin_1/training_1/train/hdf5/training.hdf5"
VAL_HDF5   = "/home/prabu-test/MachineLearning/Akhil/coinDemo/dataset/coin_1/training_1/validation/hdf5/validation.hdf5"
