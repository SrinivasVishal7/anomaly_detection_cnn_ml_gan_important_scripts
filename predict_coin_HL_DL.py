import argparse
# import progressbar
# import random
import os
import pickle
from sklearn.externals import joblib
import cv2
import imutils
import numpy as np
# import the necessary packages
from imutils import paths
from keras.models import load_model

from cnn_module.pyimagesearch.preprocessing import ExtractPreprocessorTwoImagesBlur as EP

### Multiprocessing

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()

ap.add_argument("-o", "--output", default=None,
                help="path to model")
ap.add_argument("-mh", "--model_HL", required=True,
                help="path to model")
ap.add_argument("-md", "--model_DL", required=True,
                help="path to model")
ap.add_argument("-r", "--ref", default=None,
                help="path to model")
ap.add_argument("-a","--anomaly",required=True,
                help="Path to anomaly model")
args = vars(ap.parse_args())

model_HL = load_model(args["model_HL"])
model_DL = load_model(args["model_DL"])
lsanomaly_model = joblib.load(args["anomaly"])

fileName = args["model_HL"].split(".")
print('fileName[-2]  : ', fileName[-2])
dirName = fileName[-2].split(os.path.sep)
dirName[-1] = "ClassLabels"
dirName = os.path.sep.join(dirName)
print('dirName : ', dirName)
# fileName [-1] = "cpickle"
fileName = ".".join([dirName, "cpickle"])

f = open(fileName, "rb")
labels = pickle.load(f)
anomaly_label = ['Coin', 'Not a Coin']
print('classes : ', len(labels))
score = [None] * (len(labels))
print('labels : ', labels)
f.close()

refImages = [None] * (len(labels))
refImgCount = 0
for lb in labels:
    refPath = os.path.sep.join([args['ref'], lb])
    refImgPath = list(paths.list_images(refPath))
    refImages[refImgCount] = cv2.imread(refImgPath[0])
    refImgCount += 1

crop_w = 144
crop_h = 144

WIDTH = 640
HEIGHT = 480

normalizeCam = False
MARGIN = 50

camFront = cv2.VideoCapture(0)
if not camFront.isOpened():
    print('camera not opened')
    exit()
else:
    print('camera opened')

camFront.set(cv2.CAP_PROP_FRAME_WIDTH, WIDTH)
camFront.set(cv2.CAP_PROP_FRAME_HEIGHT, HEIGHT)

cv2.namedWindow('Full Image front', cv2.WINDOW_NORMAL)
cv2.resizeWindow('Full Image front', int(WIDTH / 2), int(HEIGHT / 2))

imageCount = 0
thresVal = 1
whiteOnBblack = True


def imshowByN(name, image, by=2):
    cv2.imshow(name,
               cv2.resize(image, (int(image.shape[1] / by), int(image.shape[0] / by)), interpolation=cv2.INTER_CUBIC))


def takeBackground():
    while True:
        ret, image_front = camFront.read()
        if ret is False:
            print("image not found")
            exit()

        image_front = imutils.rotate(image_front, 180)
        imshowByN('Full Image front', image_front, 3)

        k = cv2.waitKey(1)
        if chr(k & 255) is 'q':
            exit()
        elif chr(k & 255) is 'b':
            return image_front


image_front = takeBackground()

reqImgW = int(len(image_front[0]))
reqImgH = int(len(image_front) / 2) + 30

cv2.namedWindow('threshold Image', cv2.WINDOW_NORMAL)
cv2.resizeWindow('threshold Image', int(reqImgW / 2), int(reqImgH / 2))

imgX, imgY = int(image_front.shape[1] / 2), int(image_front.shape[0] / 2)

croppedImageFront = image_front[imgY - int(reqImgH / 2):imgY + int(reqImgH / 2),
                    imgX - int(reqImgW / 2):imgX + int(reqImgW / 2)]

first_gray_front = cv2.cvtColor(croppedImageFront, cv2.COLOR_BGR2GRAY)

# extImage = EP(GRAYBaseFrameFrontCam=first_gray_front, GRAYBaseFrameTopCam=first_gray_top, thresholdValue=thresVal, H=crop_h, W=crop_w)
extImage = EP(GRAYBaseFrame=first_gray_front, thresholdValue=thresVal, H=crop_h, W=crop_w, whiteOnBblack=whiteOnBblack)


def fineTunePrediction(image):
    # canvas = np.zeros((35*len(labels)+5, 300, 3), dtype="uint8")
    canvas = np.zeros((35 * 20 + 5, 350 * 2, 3), dtype="uint8")
    croppedImage = image.copy()
    image = np.expand_dims(image, axis=0)
    features = model_HL.predict(image, verbose=0)
    lsanomaly_feature = features.flatten()
    lsanomaly_feature = np.array(lsanomaly_feature)
    lsanomaly_feature = lsanomaly_feature.reshape(1, -1)
    print(lsanomaly_feature)
    anomaly_preds = lsanomaly_model.predict_proba(lsanomaly_feature)
    print (anomaly_preds)
    anomaly_pred = np.argmax(anomaly_preds, axis=1)
    print(anomaly_pred[0])

    if True:
        print(anomaly_pred[0])
        preds = model_DL.predict(features, verbose=0)
        pred = np.argmax(preds, axis=1)
        for cl in range(len(labels)):
            val = preds[0][cl] * 100
            score[cl] = round(val, 2)
        best_score = score[int(pred)]
        cv2.putText(tempImageFront, labels[int(pred)] + ' ' + str(best_score), (300, 50), 0, 0.8, (0, 0, 255), 4,
                    cv2.LINE_AA)
        imshowByN('reference image', refImages[int(pred)], 1)
    else:
        cv2.putText(tempImageFront, anomaly_label[int(anomaly_pred)], (300, 50), 0, 0.8, (255, 0, 0), 2, cv2.LINE_AA)

    # cv2.imshow ('croppedImage', croppedImage)
    # cv2.imshow ('probabilities', canvas)
    # cv2.imshow ('frame', fullImage)
    # cv2.resizeWindow ('frame', int(1280/4), int(720/4))
    # cv2.imshow ('reference image', refImages[int(pred)])

while True:
    ret, image_front = camFront.read()
    if ret is False:
        print("image not found")
        exit()

    image_front = imutils.rotate(image_front, 180)

    tempImageFront = image_front.copy()
    tempImageFront[imgY - int(reqImgH / 2), imgX - int(reqImgW / 2):imgX + int(reqImgW / 2)] = 255
    tempImageFront[imgY + int(reqImgH / 2), imgX - int(reqImgW / 2):imgX + int(reqImgW / 2)] = 255
    tempImageFront[imgY - int(reqImgH / 2):imgY + int(reqImgH / 2), imgX + int(reqImgW / 2) - 1] = 255
    tempImageFront[imgY - int(reqImgH / 2):imgY + int(reqImgH / 2), imgX - int(reqImgW / 2)] = 255

    image_front = image_front[imgY - int(reqImgH / 2):imgY + int(reqImgH / 2),
                  imgX - int(reqImgW / 2):imgX + int(reqImgW / 2)]
    nextBgImageFront = image_front.copy()

    status, thresholdFullImage, _ = extImage.preprocess(image_front)
    #	totalNumImg = len(extractedImageList)

    # cv2.imshow ('threshold Image', thresholdFullImage)
    imshowByN('threshold Image', thresholdFullImage, 1)
    fineTunePrediction(thresholdFullImage)
    '''
	if biggestPillImage is not None:
		imshowByN ('biggestPillImage', biggestPillImage, 1)
		if np.asarray (biggestPillImage).shape == (224, 224, 3):
			startTime = datetime.datetime.now()
			fineTunePrediction(biggestPillImage)
			endTime = datetime.datetime.now()
	
	if len(extractedImageList) is not 0:
		#cv2.imshow ('croppedImages', np.concatenate(extractedImageList, axis=1))
		imshowByN ('croppedImages', np.concatenate(extractedImageList, axis=1), 2)
	'''
    # cv2.imshow ('Full Image front', tempImageFront)
    imshowByN('Full Image front', tempImageFront, 1)

    k = cv2.waitKey(1)
    if chr(k & 255) is 'q':
        break
    elif chr(k & 255) is 'b':
        first_gray_front = cv2.cvtColor(nextBgImageFront, cv2.COLOR_BGR2GRAY)

        extImage = EP(GRAYBaseFrame=first_gray_front, thresholdValue=thresVal, H=crop_h, W=crop_w,
                      whiteOnBblack=whiteOnBblack)
        #		extImage = EP(GRAYBaseFrameFrontCam=first_gray_front, GRAYBaseFrameTopCam=first_gray_top, thresholdValue=thresVal, H=crop_h, W=crop_w)
        cv2.destroyAllWindows()

        cv2.namedWindow('Full Image front', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('Full Image front', int(WIDTH / 2), int(HEIGHT / 2))
        cv2.namedWindow('threshold Image', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('threshold Image', int(reqImgW / 2), int(reqImgH / 2))

    elif chr(k & 255) is 'v':
        thresVal = int(input("Enter threshold : "))
        extImage = EP(GRAYBaseFrame=first_gray_front, thresholdValue=thresVal, H=crop_h, W=crop_w,
                      whiteOnBblack=whiteOnBblack)
#		extImage = EP(GRAYBaseFrameFrontCam=first_gray_front, GRAYBaseFrameTopCam=first_gray_top, thresholdValue=thresVal, H=crop_h, W=crop_w)
