import argparse
import csv
import os

import numpy as np
from keras.applications import MobileNet
from keras.applications.imagenet_utils import preprocess_input
from keras.models import load_model
from keras.preprocessing import image
from keras_squeezenet import SqueezeNet
from sklearn.externals import joblib

ap = argparse.ArgumentParser()

ap.add_argument("-p", "--path", required=True,
                help="path to the test images")

ap.add_argument("-n", "--name", required=True,
                help="path to the saved model")

ap.add_argument("-m", "--model", default="0",
                help="0-mobilenet model, 1-squeezenet model, 2-Custom path for model")

ap.add_argument("-mh", "--model_path", default=None, help="custom model path for feature extraction")

ap.add_argument("-md","--fc", required=True, help="Fully connected layer model")

ap.add_argument("-c", "--csv", required=True,
                help="path to CSV file with name")

args = vars(ap.parse_args())

if args["model"] is "2" and args["model_path"] is None :
    raise Exception("Give model path")
path = args["path"]

model = {
    "0": MobileNet(input_shape=(128, 128, 3), weights='imagenet', include_top=False),
    "1": SqueezeNet(input_shape=(128, 128, 3), weights='imagenet', include_top=False),
    "2": load_model(args["model_path"])

}

fc_model = load_model(args["fc"])


print("\nLoading pre-trained model...\n")

print("\nReading images from '{}' directory...\n".format(path))

lsanomaly = joblib.load(args["name"])

f = open(args["csv"], 'a')
writer = csv.DictWriter(f, fieldnames=['File Name', 'Value'])
writer.writeheader()


for subdir, dirs, files in os.walk(path):
    for filename in files:
        if filename.endswith(".bmp"):
            filename_full = os.path.join(subdir, filename)
            img = image.load_img(filename_full)
            img = image.img_to_array(img)
            img = np.expand_dims(img, axis=0)
            img = preprocess_input(img)
            features = model[args["model"]].predict(img).flatten()
            Y = np.array(features)
            Y = Y.reshape(1, -1)
            prediction = lsanomaly.predict_proba(Y)

            print("\n---------------------------------------------------------------")
            print("\nFeature vectors obtained for : {}".format(filename))
            print("\n---------------------------------------------------------------")
            print("\nPrediction for {} : {}".format(filename, prediction))
            print("\n----------------------------------------------------------------")
            writer.writerow({'File Name': filename_full, 'Value': prediction})
