import argparse
import cv2
import os

import numpy as np
from keras.applications import VGG16
from keras.applications.imagenet_utils import preprocess_input
from keras.applications.mobilenet import MobileNet
from keras.preprocessing import image as image_preprocessor
from keras_squeezenet.squeezenet import SqueezeNet

ap = argparse.ArgumentParser()

ap.add_argument("-p", "--path", required=True,
                help="path to the train images")

ap.add_argument("-m", "--model", default="0",
                help="0-mobilenet model, 1-squeezenet model, 2-Custom path for model")

ap.add_argument("-mp", "--model_path", default=None, help="custom model path for feature extraction")

ap.add_argument("-n", "--name", required=True,
                help="path to the saved features")

args = vars(ap.parse_args())

if args["model"] is "3" and args["model_path"] is None:
    raise Exception("Give model path")

path = args["path"]

model = {
    "0": MobileNet(input_shape=(128, 128, 3), weights='imagenet', include_top=False),
    "1": SqueezeNet(input_shape=(128, 128, 3), weights='imagenet', include_top=False),
    "2": VGG16(input_shape=(224, 224, 3), weights='imagenet', include_top=False),
    #"3": load_model(args["model_path"])
}

print("\nLoading pre-trained model...\n")

print("\nReading images from '{}' directory...\n".format(path))

final_features = []
count = -1

for subdir, dirs, files in os.walk(path):
    for filename in files:
        if filename.endswith(".bmp"):
            filename_full = os.path.join(subdir, filename)
            img = cv2.imread(filename_full)
            img = cv2.resize(img, (224, 224), cv2.INTER_AREA)
            img = np.expand_dims(img, axis=0)
            img = img.astype('float64')
            img = preprocess_input(img)
            features = model[args["model"]].predict(img).flatten()
            print(features.shape)
            final_features.append(features)

final_features = np.array(final_features)
print(final_features.shape)
np.save(args["name"], final_features)
