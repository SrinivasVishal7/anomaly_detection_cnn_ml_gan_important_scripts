import os

import cv2
import matplotlib.pyplot as plt
import numpy as np
from keras.applications import VGG16
from keras.applications.imagenet_utils import preprocess_input
from keras.preprocessing import image
from lsanomaly import LSAnomaly
from skimage.color import label2rgb
from sklearn.externals import joblib
from sklearn.pipeline import Pipeline

coin_model = LSAnomaly()
feature_extractor = VGG16(weights='imagenet', include_top=False)


class PipeStep(object):
    def __init__(self, step_func):
        self._step_func = step_func

    def fit(self, *args):
        return self

    def transform(self, X):
        return self._step_func(X)


path = '/home/jelari-apps/training_5/train/Five'

image_dataset = []

for subdir, dirs, files in os.walk(path):
    for filename in files:
        if filename.endswith(".bmp"):
            filename_full = os.path.join(subdir, filename)
            img = cv2.imread(filename_full)
            img = cv2.resize(img, (224, 224), cv2.INTER_AREA)
            image_dataset.append(img)

image_dataset = np.array(image_dataset)
print(image_dataset.shape)


def transform_img_fn(img):
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    preprocessed = preprocess_input(x)
    return preprocessed


def extract_feature(preprocessed):
    feature = feature_extractor.predict(preprocessed).flatten()
    feature = np.array(feature)
    return feature


process_img = PipeStep(lambda img_list: [transform_img_fn(img) for img in img_list])
convert_img_to_feature = PipeStep(lambda preprocessed_images: np.array([extract_feature(preprocessed_image) for preprocessed_image in preprocessed_images]))

coin_pipeline = Pipeline([
    ('Process image', process_img),
    ('Make feature', convert_img_to_feature),
    ('coin model', coin_model)
])

labels = np.array([0]*image_dataset.shape[0])
print(labels.shape)

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(image_dataset, labels,
                                                    train_size=0.55)
print(X_train.shape)
print(X_train[0].shape)
print(y_test[0])

coin_pipeline.fit(X_train, y_train)

from lime import lime_image
from lime.wrappers.scikit_image import SegmentationAlgorithm

explainer = lime_image.LimeImageExplainer(verbose=True)
segmenter = SegmentationAlgorithm('quickshift', kernel_size=1, max_dist=200, ratio=0.2)

explanation = explainer.explain_instance(X_test[0],
                                         classifier_fn=coin_pipeline.predict_proba, segmentation_fn=segmenter)

temp, mask = explanation.get_image_and_mask(y_test[0], positive_only=True, num_features=10, hide_rest=False, min_weight = 0.01)

fig, (ax1, ax2) = plt.subplots(1,2, figsize = (8, 4))

ax1.imshow(label2rgb(mask,temp, bg_label = 0), interpolation = 'nearest')

ax1.set_title('Positive Regions for {}'.format(y_test[0]))

temp, mask = explanation.get_image_and_mask(y_test[0], positive_only=False, num_features=10, hide_rest=False, min_weight = 0.01)

ax2.imshow(label2rgb(3-mask,temp, bg_label = 0), interpolation = 'nearest')

ax2.set_title('Positive/Negative Regions for {}'.format(y_test[0]))

plt.show()
