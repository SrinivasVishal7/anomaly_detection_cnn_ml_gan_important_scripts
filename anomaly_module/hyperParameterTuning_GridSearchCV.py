import argparse
import os

import numpy as np
from lsanomaly import LSAnomaly
from sklearn.model_selection import GridSearchCV

ap = argparse.ArgumentParser()


ap.add_argument("-p", "--feature", required=True,
                help="path to the train images")

ap.add_argument("-d","--dataset", default=None,
                help="dataset path for class labelling")

ap.add_argument("-m","--mode",default=None,
                help="0 - ")

args = vars(ap.parse_args())
feature_path = args["feature"]
dataset_path = args["dataset"]

cpt = [len(files) for r, d, files in os.walk(dataset_path)]

count = cpt[1:]
labels = []
label_counter = 0

for i in count:
    label = [label_counter]*i
    labels.append(label)
    label_counter += 1


labels = np.array(labels).flatten()
print("Labels shape : {}".format(labels.shape))
print("Labels : {}".format(labels))
print("Classes : {}".format(count))

features = np.load(feature_path)
print(features.shape)
sigmas = np.arange(400, 800, 5, dtype=float)
rhos = np.arange(0.1, 10, 0.2, dtype=float)
model = LSAnomaly()
grid = GridSearchCV(estimator=model, param_grid=dict(sigma=sigmas, rho=rhos))
grid.fit(features, labels)
print("Grid : {}".format(grid))
print("Best Score : {}".format(grid.best_score_))
print("Best sigma : {}".format(grid.best_estimator_.sigma))
print("Best rho : {]".format(grid.best_estimator_.rho))
