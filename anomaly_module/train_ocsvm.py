import numpy as np
from sklearn.svm.classes import OneClassSVM
import argparse
from sklearn.externals import joblib
import os

ap = argparse.ArgumentParser()

ap.add_argument("-p", "--feature", required=True,
                help="path to the train images")

ap.add_argument("-m", "--model", default="0",
                help="model name with path")

ap.add_argument("-nu", "--neu", default=0.1)

ap.add_argument("-gam", "--gamma", required=False, default=None)

ap.add_argument("-d", "--dataset", default=None,
                help="dataset path for class labelling")

args = vars(ap.parse_args())
feature_path = args["feature"]
model_path = args["model"]
nu = float(args["neu"])
gamma = args["gamma"]

dataset_path = args["dataset"]

if gamma is None:
    ocsvm = OneClassSVM(gamma=0.001, kernel='rbf', nu=0.08)

else:
    ocsvm = OneClassSVM(gamma=float(gamma), kernel='rbf', nu=nu)

features = np.load(feature_path)
print(features.shape)

if dataset_path is not None:
    cpt = [len(files) for r, d, files in os.walk(dataset_path)]

    count = cpt[1:]
    labels = []
    label_counter = 0

    for i in count:
        label = [label_counter] * i
        labels.append(label)
        label_counter += 1

    labels = np.array(labels).flatten()
    print("Labels shape : {}".format(labels.shape))
    print("Labels : {}".format(labels))
    print("Classes : {}".format(count))

    ocsvm.fit(features, y=labels)

else:
    ocsvm.fit(features)

joblib.dump(ocsvm, model_path)
