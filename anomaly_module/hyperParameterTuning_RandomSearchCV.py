import numpy as np
from scipy.stats import uniform as sp_rand
from sklearn import datasets
from lsanomaly import LSAnomaly
from sklearn.model_selection import RandomizedSearchCV

dataset = datasets.load_diabetes()
param_grid = {'sigma': sp_rand(), 'rho': sp_rand()}
model = LSAnomaly()
rsearch = RandomizedSearchCV(estimator=model, param_distributions=param_grid, n_iter=100)
rsearch.fit(dataset.data, dataset.target)
print(rsearch)
print(dataset.target.shape)
print(rsearch.best_score_)
print(rsearch.best_estimator_.sigma)
print(rsearch.best_estimator_.rho)
