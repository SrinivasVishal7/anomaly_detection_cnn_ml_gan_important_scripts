import argparse

import numpy as np
import optunity
import optunity.metrics
import sklearn
from lsanomaly import LSAnomaly

ap = argparse.ArgumentParser()

ap.add_argument("-p", "--feature", required=True,
                help="path to the train images")

args = vars(ap.parse_args())

feature_path = args["feature"]

features = np.load(feature_path)
print(features.shape)

data = features
negative_features = np.load('/home/hackathon/vision_project/features/New_features/Foreign/Foreign_1000.npy')


def lsAnomaly_tuned_auroc(x_train, x_test, sigma, rho):
    combined_features = np.vstack((x_test, negative_features))
    np.random.shuffle(negative_features)
    print("Train features shape : {}".format(x_train.shape))
    print("Test features shape : {}".format(x_test.shape))
    print("Negative features shape : {}".format(negative_features.shape))
    print("Combined features shape : {}".format(combined_features.shape))
    labels_positive = np.ones(x_test.shape[0], dtype=int)
    labels_negative = np.zeros(negative_features.shape[0], dtype=int)
    labels = np.append(labels_positive, labels_negative)
    print("Positive labels shape: {}".format(labels_positive.shape))
    print("Negative labels shape: {}".format(labels_negative.shape))
    print("Combined labels shape: {}".format(labels.shape))
    model = LSAnomaly(sigma=sigma, rho=rho, n_kernels_max=1000)
    model.fit(x_train)
    print(combined_features.shape)
    decision_values = model.decision_function(combined_features)
    print("Decision Values : {}".format(decision_values))
    auc_roc = sklearn.metrics.roc_auc_score(labels, decision_values)
    print("AUROC score for samples : {}".format(auc_roc))
    return auc_roc


cv_decorator = optunity.cross_validated(x=data)

lsAnomaly_tuned_auroc = cv_decorator(lsAnomaly_tuned_auroc)

optimal_rbf_pars, info, _ = optunity.maximize(lsAnomaly_tuned_auroc, num_evals=100, sigma=[200, 900], rho=[0.01, 20])

print("Optimal parameters: " + str(optimal_rbf_pars))
print("AUROC of tuned LSAnomaly model : %1.3f" % info.optimum)
