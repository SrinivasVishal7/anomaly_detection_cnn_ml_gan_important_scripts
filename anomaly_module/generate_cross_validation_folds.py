import numpy as np
from optunity import generate_folds
from optunity.cross_validation import strata_by_labels
from optunity.metrics import roc_auc

# folds = generate_folds(num_rows=2000, num_folds=2, clusters=[list(range(0, 1000))])
#
# print(folds[1])
# print(folds[0])

print(roc_auc([0, 0, 1, 1, 0, 1, 0], [0, 0, 1, 1, 1, 1, 0]))

