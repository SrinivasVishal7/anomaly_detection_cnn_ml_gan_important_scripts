import sklearn

import optunity
import optunity.metrics
from sklearn.svm.classes import OneClassSVM
import argparse
import os

import numpy as np
from lsanomaly import LSAnomaly

ap = argparse.ArgumentParser()

ap.add_argument("-p", "--feature", required=True,
                help="path to the train images")

args = vars(ap.parse_args())
feature_path = args["feature"]

features = np.load(feature_path)
print(features.shape)

data = features[:, :-1]
labels = features[:, -1]
print(data.shape)
print(labels.shape)
print(labels)


def lsAnomaly_tuned_auroc(x_train, y_train, x_test, y_test, sigma, rho):
    print(x_train.shape)
    print(y_test.shape)
    model = LSAnomaly(sigma=sigma, rho=rho)
    model.fit(x_train, y_train)
    decision_values = model.decision_function(x_test)
    auc_sklearn = sklearn.metrics.roc_auc_score(y_test, decision_values)
    print("AUROC Sklearn score : {}".format(auc_sklearn))
    return auc_sklearn


cv_decorator = optunity.cross_validated(x=data, y=labels)

lsAnomaly_tuned_auroc = cv_decorator(lsAnomaly_tuned_auroc)

optimal_rbf_pars, info, _ = optunity.maximize(lsAnomaly_tuned_auroc, num_evals=100, sigma=[200, 900], rho=[0.01, 20])

print("Optimal parameters: " + str(optimal_rbf_pars))
print("AUROC of tuned SVM : %1.3f" % info.optimum)
