from lsanomaly import LSAnomaly
from sklearn.externals import joblib
import argparse

ap = argparse.ArgumentParser()

ap.add_argument("-p", "--path", required=True,
                help="path to the train images")

ap.add_argument("-mp", "--model_path", default=None, help="custom model path for feature extraction")

args = vars(ap.parse_args())