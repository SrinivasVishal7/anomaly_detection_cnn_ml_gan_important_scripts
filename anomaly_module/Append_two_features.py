import numpy as np
from lsanomaly import LSAnomaly
import argparse
from sklearn.externals import joblib
import os

ap = argparse.ArgumentParser()

ap.add_argument("-p1", "--feature1", required=True,
                help="path to feature 1")

ap.add_argument("-p2", "--feature2", required=True,
                help="path to feature 2")

ap.add_argument("-n", "--name", required=True,
                help="path to the saved features")

args = vars(ap.parse_args())
feature_path_1 = args["feature1"]
feature_path_2 = args["feature2"]

features1 = np.load(feature_path_1)
print(features1.shape)
features2 = np.load(feature_path_2)
print(features2.shape)

features3 = np.vstack((features1, features2))
print(features3.shape)
np.save(args["name"], features3)

