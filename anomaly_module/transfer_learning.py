import argparse

import numpy as np
from keras.layers import Dropout, Flatten, Dense
from keras.models import Sequential

img_width, img_height = 128, 128

top_model_weights_path = 'coin_fc_model.h5'
train_data_dir = 'data/train'
validation_data_dir = 'data/validation'
nb_train_samples = 2000
nb_validation_samples = 800
epochs = 50
batch_size = 16

ap = argparse.ArgumentParser()

ap.add_argument("-p", "--feature", required=False,
                help="path to the train images")

ap.add_argument("-m", "--model", default="0",
                help="model name with path")


def train_top_model():
    train_data = np.load()
    train_labels = np.array(
        [0] * int((nb_train_samples / 2)) + [1] * int((nb_train_samples / 2)))

    validation_data = np.load(open('bottleneck_features_validation.npy'))
    validation_labels = np.array(
        [0] * int((nb_validation_samples / 2)) + [1] * int((nb_validation_samples / 2)))

    model = Sequential()
    model.add(Flatten(input_shape=train_data.shape[1:]))
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(4, activation='softmax'))

    model.compile(optimizer='rmsprop',
                  loss='categorical_crossentropy', metrics=['accuracy'])

    model.fit(train_data, train_labels,
              epochs=epochs,
              batch_size=batch_size,
              validation_data=(validation_data, validation_labels))

    model.save_weights(top_model_weights_path)


train_top_model()