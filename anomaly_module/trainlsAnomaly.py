import numpy as np
from lsanomaly import LSAnomaly
import argparse
from sklearn.externals import joblib
import os

ap = argparse.ArgumentParser()

ap.add_argument("-p", "--feature", required=True,
                help="path to the train images")

ap.add_argument("-m", "--model", default="0",
                help="model name with path")

ap.add_argument("-rho", "--rho", default=0.1)

ap.add_argument("-sig", "--sigma", required=False, default=None)

ap.add_argument("-d", "--dataset", default=None,
                help="dataset path for class labelling")

args = vars(ap.parse_args())
feature_path = args["feature"]
model_path = args["model"]
rho = float(args["rho"])
sigma = args["sigma"]

dataset_path = args["dataset"]

if sigma is None:
    lsanomaly = LSAnomaly()

else:
    lsanomaly = LSAnomaly(sigma=float(sigma), rho=rho, n_kernels_max=1000)

features = np.load(feature_path)
print(features.shape)

if dataset_path is not None:
    cpt = [len(files) for r, d, files in os.walk(dataset_path)]

    count = cpt[1:]
    labels = []
    label_counter = 0

    for i in count:
        label = [label_counter] * i
        labels.append(label)
        label_counter += 1

    labels = np.array(labels).flatten()
    print("Labels shape : {}".format(labels.shape))
    print("Labels : {}".format(labels))
    print("Classes : {}".format(count))

    lsanomaly.fit(features, y=labels)

else:
    lsanomaly.fit(features)

joblib.dump(lsanomaly, model_path)
