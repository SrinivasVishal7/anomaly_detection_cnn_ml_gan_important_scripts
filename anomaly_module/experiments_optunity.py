import argparse
import sklearn

import numpy as np
import optunity
import optunity.metrics
from lsanomaly import LSAnomaly

ap = argparse.ArgumentParser()

ap.add_argument("-p", "--feature", required=True,
                help="path to the train images")

args = vars(ap.parse_args())

feature_path = args["feature"]

features = np.load(feature_path)
print(features.shape)

data = features
print(data.shape)


def lsAnomaly_tuned_auroc(x_train, x_test, sigma, rho):
    print(x_train.shape)
    print(x_test.shape)
    model = LSAnomaly(sigma=sigma, rho=rho)
    model.fit(x_train)
    decision_values = model.predict(x_test)
    print(decision_values)
    decision_values = [1 if x == 'anomaly' else int(x) for x in decision_values]
    labels = np.zeros(x_test.shape[0], dtype=int).tolist()
    print(decision_values)
    print(labels)
    acc_optunity = sklearn.metrics.accuracy_score(labels, decision_values)
    print("AUROC Optunity score : {}".format(acc_optunity))
    return acc_optunity


cv_decorator = optunity.cross_validated(x=data)

lsAnomaly_tuned_auroc = cv_decorator(lsAnomaly_tuned_auroc)

optimal_rbf_pars, info, _ = optunity.maximize(lsAnomaly_tuned_auroc, num_evals=100, sigma=[200, 900], rho=[0.01, 20])

print("Optimal parameters: " + str(optimal_rbf_pars))
print("AUROC of tuned SVM : %1.3f" % info.optimum)
