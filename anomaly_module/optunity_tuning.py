import optunity
import optunity.metrics
from sklearn.svm.classes import OneClassSVM
from sklearn.cross_validation import train_test_split
import argparse
import os

import numpy as np
from lsanomaly import LSAnomaly

ap = argparse.ArgumentParser()

ap.add_argument("-p", "--feature", required=True,
                help="path to the train images")


args = vars(ap.parse_args())
feature_path = args["feature"]
dataset_path = args["dataset"]

features = np.load(feature_path)
print(features.shape)

cpt = [len(files) for r, d, files in os.walk(dataset_path)]

print(cpt)

if args["mode"]:
    count = cpt[1:]
    labels = []
    label_counter = 0

    for i in count:
        label = [label_counter] * i
        labels.append(label)
        label_counter += 1

    labels = np.array(labels).flatten()
    print("Labels shape : {}".format(labels.shape))
    print("Labels : {}".format(labels))
    print("Classes : {}".format(count))

else:
    labels = np.ones(features.shape[0])


def lsAnomaly_tuned_auroc(x_train, y_train, x_test, y_test, sigma, rho):
    print(x_train.shape)
    print(y_test.shape)
    model = LSAnomaly(sigma=sigma, rho=rho).fit(x_train, y_train)
    decision_values = model.decision_function(x_test)
    auc = optunity.metrics.roc_auc(y_test, decision_values)
    return auc


cv_decorator = optunity.cross_validated(x=features, y=labels, num_folds=10, num_iter=5)

lsAnomaly_tuned_auroc = cv_decorator(lsAnomaly_tuned_auroc)

optimal_rbf_pars, info, _ = optunity.maximize(lsAnomaly_tuned_auroc, num_evals=300, sigma=[100, 1000], rho=[0.1, 20])

print("Optimal parameters: " + str(optimal_rbf_pars))
print("AUROC of tuned LsAnomaly : %1.3f" % info.optimum)
